import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API = 'http://localhost:4300';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  // Request: categoria pela ID
  getCategoryById(id: number) {
    return this.http.get<Object[]>(API + '/categories?id=' + id);
  }

  // Request: categoria pelo nome
  getCategoryByName(name: string) {
    return this.http.get<Object[]>(API + '/categories?name=' + name);
  }

  // Request: categoria pelo path
  getCategoryByPath(path: string) {
    return this.http.get<Object[]>(API + '/categories?path=' + path);
  }

  // Request: todas as categorias
  getCategories() {
    return this.http.get<Object[]>(API + '/categories');
  }

}