import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Product } from './product';

const API = 'http://localhost:4300';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  // Request: produto pelo ID
  getProduct(id: number) {
    return this.http
      .get<Product[]>(API + `/products?id=${id}`);
  }

  // Request: Lista de produtos por categoria
  // @cat: pode ser ID ou path da categoria
  getProductsByCategory(cat: any, page: number, limit: number, sort: string, order: string) {
    return this.http
      .get<Product[]>(API + `/products?category=${cat}&_page=${page}&_limit=${limit}&_sort=${sort}&_order=${order}`);
  }

  // Request: todos os produtos
  getAllProducts() {
    return this.http
      .get<Product[]>(API + '/products');
  }

}