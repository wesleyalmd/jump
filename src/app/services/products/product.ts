export interface Product {
  id: number,
  stock: number,
  path: string,
  name: string,
  price: number,
  old_price: any,
  thumbnail: string,
  gallery: string[],
  attributes: Object[],
  short_description: string,
  related: number[],
  category: string[]
}
