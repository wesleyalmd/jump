import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationModule } from './navigation/navigation.module';
import { ProductModule } from './product/product.module';
import { CatalogModule } from './catalog/catalog.module';

@NgModule({
  imports: [
    CommonModule,
    NavigationModule,
    ProductModule,
    CatalogModule
  ],
  declarations: [],
  exports: [
    NavigationModule,
    ProductModule,
    CatalogModule
  ],
})
export class ComponentsModule { }
