import { Component, OnInit, Input } from '@angular/core';

import { Product } from '../../../services/products/product';

@Component({
  selector: 'product-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() product: Product = null;

  constructor() { }

  ngOnInit() {
  }

}
