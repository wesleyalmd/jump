import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ThumbnailComponent } from './thumbnail/thumbnail.component';
import { CardComponent } from './card/card.component';
import { GalleryComponent } from './gallery/gallery.component';
import { DescriptionComponent } from './description/description.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { RelatedComponent } from './related/related.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    ThumbnailComponent, 
    CardComponent, 
    GalleryComponent, 
    DescriptionComponent, 
    ReviewsComponent, 
    RelatedComponent
  ],
  exports: [
    ThumbnailComponent, 
    CardComponent, 
    GalleryComponent, 
    DescriptionComponent, 
    ReviewsComponent, 
    RelatedComponent
  ]
})
export class ProductModule { }
