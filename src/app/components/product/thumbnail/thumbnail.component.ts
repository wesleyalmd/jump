import { Component, OnInit, Input } from '@angular/core';

import { Product } from '../../../services/products/product';

@Component({
  selector: 'product-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.scss']
})
export class ThumbnailComponent implements OnInit {

  @Input() product: Product = null;

  constructor() { }

  ngOnInit() {
  }

}
