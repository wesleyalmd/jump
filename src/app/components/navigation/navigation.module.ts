import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { FiltersComponent } from './filters/filters.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { PaginationComponent } from './pagination/pagination.component';
import { SearchComponent } from './search/search.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
    FiltersComponent, 
    ToolbarComponent, 
    PaginationComponent, 
    SearchComponent, 
    BreadcrumbComponent
  ],
  exports: [
    FiltersComponent, 
    ToolbarComponent, 
    PaginationComponent, 
    SearchComponent, 
    BreadcrumbComponent
  ]
})
export class NavigationModule { }
