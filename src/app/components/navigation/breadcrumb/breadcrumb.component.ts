import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'navigation-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  @Input() bcrumbs: Object[] = []
  @Input() current: string;

  constructor() { }

  ngOnInit() {
  }

}
