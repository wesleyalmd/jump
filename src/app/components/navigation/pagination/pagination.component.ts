import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'navigation-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  pages: number[] = [1, 2, 3];
  prev: boolean = false;
  @Input() next: boolean = true;

  @Output() pageCurrent: number = 1;
  @Output() pageChange = new EventEmitter;

  constructor() { }

  changePage(value) {
    this.pageChange.emit({ page: value })
  }

  pagePrev() {
    this.pageCurrent = (this.pageCurrent >= 2) ? this.pageCurrent - 1 : this.pageCurrent;
    this.prev = (this.pageCurrent <= 1) ? false : true;
  }

  pageNext() {
    this.pageCurrent = (this.next) ? this.pageCurrent + 1 : this.pageCurrent;
    this.prev = (this.pageCurrent >= 2) ? true : false;
  }

  ngOnInit() {
  }

}
