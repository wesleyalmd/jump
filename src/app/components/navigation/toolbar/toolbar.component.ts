import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'navigation-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  sortOptions: Object[] = [
    { value: 'news', label: 'Lançamentos' },
    { value: 'price-min', label: 'Menor preço' },
    { value: 'price-max', label: 'Maior preço' }
  ];

  @Input() showOptions: number[] = [12, 16, 24];
  @Input() showCurrent: number = this.showOptions[0];
  @Input() viewMode: string = 'grid';
  
  @Output() showChange = new EventEmitter;
  @Output() sortChange = new EventEmitter;
  @Output() viewChange = new EventEmitter;

  constructor() { }

  changeShow(value) {
    this.showChange.emit({ show: value })
    console.log(value);
  }

  changeSort(value) {
    this.sortChange.emit({ sort: value })
    console.log(value);
  }

  changeView(value) {
    this.viewMode = value;
    this.viewChange.emit({ view: value })
    console.log(value);
  }

  ngOnInit() {
    
  }

}
