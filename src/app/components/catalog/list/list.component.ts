import { Component, OnInit, Input } from '@angular/core';

import { Product } from '../../../services/products/product';

@Component({
  selector: 'catalog-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() products: Product[] = [];
  @Input() viewMode: string = 'grid';

  constructor() { }

  ngOnInit() {
    
  }

}
