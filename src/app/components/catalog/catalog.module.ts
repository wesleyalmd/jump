import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductModule } from '../product/product.module';

import { ListComponent } from './list/list.component';
import { CarouselComponent } from './carousel/carousel.component';

@NgModule({
  imports: [
    CommonModule,
    ProductModule
  ],
  declarations: [
    ListComponent, 
    CarouselComponent
  ],
  exports: [
    ListComponent, 
    CarouselComponent
  ]
})
export class CatalogModule { }
