import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() logo: string = '';

  hamburger: boolean = false;

  showSearch: boolean = true;

  showNavbar: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
