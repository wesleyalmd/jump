import { Component, OnInit, Input } from '@angular/core';
import { CategoriesService } from '../../services/categories/categories.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  nav: Object[] = [
    { path: '/', name: 'Home' }
  ];

  navbar = [];

  @Input() active: boolean = false;

  constructor(public service: CategoriesService) {
    service.getCategories().subscribe(data => {
      this.nav = this.nav.concat(data);
      this.navbar = new Array(this.nav.length);
    });
  }

  ngOnInit() {
  
  }

}
