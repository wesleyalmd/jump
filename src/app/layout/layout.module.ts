import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from './navbar/navbar.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavigationModule } from '../components/navigation/navigation.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NavigationModule
  ],
  declarations: [NavbarComponent, HeaderComponent, FooterComponent],
  exports: [NavbarComponent, HeaderComponent, FooterComponent]
})
export class LayoutModule { }
