import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CategoriesService } from '../../services/categories/categories.service';
import { ProductsService } from '../../services/products/products.service';
import { Product } from '../../services/products/product';

@Component({
  selector: 'page-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnChanges {

  // Estado da categoria atual
  category: string;
  categoryName: string;
  categoryId: number;
  breadcrumbs: Object[] = [];
  products: Product[] = [];

  

  // Estado dos controles da Toolbar
  viewMode: string = 'grid';
  showOptions: number[] = [8, 12, 16, 24];
  showCurrent: number = this.showOptions[0];
  sortCurrent: string = 'price';
  sortOrder: string = 'asc';

  // Estado da paginação
  pageCurrent: number = 1;
  pagePrev: boolean = false;
  pageNext: boolean = true;

  constructor(
    private routeService: ActivatedRoute,
    private categoryService: CategoriesService,
    private productService: ProductsService) {
    this.routeService.params.subscribe(route => {
      let path = route.category;
      this.createCategory(path);
      this.createCatalog(path);
    });
  }

  // Request: informações da categoria
  createCategory(route) {
    this.categoryService.getCategoryByPath(route).subscribe(data => {
      this.categoryId   = data[0]['id'];
      this.category     = data[0]['path'];
      this.categoryName = data[0]['name'];
    });
  }

  // Request: produtos com parâmtros de filtros
  createCatalog(
    cat = this.category,
    pag = this.pageCurrent,
    sho = this.showCurrent,
    sor = this.sortCurrent,
    ord = this.sortOrder) {
    this.productService.getProductsByCategory(cat, pag, sho, sor, ord)
      .subscribe(data => {
        this.products = data;
        console.log(data);
      });

    this.productService.getProductsByCategory(cat, pag + 1, sho, sor, ord)
      .subscribe(data => this.pageNext = (data.length >= 1) ? true : false);
  }

  // Trigger view mode grid/list
  viewApply(value) {
    this.viewMode = value;
  }

  // Trigger exibir
  showApply(value) {
    this.showCurrent = value;
    this.createCatalog();
  }

  // Trigger ordenar
  sortApply(value) {
    switch (value) {
      case 'price-min':
        this.sortCurrent = 'price';
        this.sortOrder = 'asc';
        break;
      case 'price-max':
        this.sortCurrent = 'price';
        this.sortOrder = 'desc';
        break;
        case 'news':
        this.sortCurrent = 'news';
        this.sortOrder = 'asc';
        break;
    }
    this.createCatalog();
  }

  // Trigger pagination
  pageApply(value) {
    this.pageCurrent = value;
    this.createCatalog();
  }

  ngOnChanges() {

  }

  ngOnInit() {

  }

}
