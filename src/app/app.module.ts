import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { CategoriesService } from './services/categories/categories.service';
import { ProductsService } from './services/products/products.service';

import { RoutingModule } from './routing.module';
import { LayoutModule } from './layout/layout.module';
import { PagesModule } from './pages/pages.module';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RoutingModule,
    LayoutModule,
    PagesModule,
    ComponentsModule
  ],
  providers: [
    // Singleton Service
    // Injecão de dependência
    CategoriesService,
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
