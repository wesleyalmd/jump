# Jump Commerce

Projeto experimental de e-commerce SPA.

### Iniciando o projeto:

Para iniciar o projeto, faça o clone do repositório

```
git clone https://bitbucket.org/wesleyalmd/jump/
```

Acesse a pasta do projeto e instale as dependências do node

```
npm install
```

Execute o projeto em modo de desenvolvimento

```
npm run start
```
Após iniciar o servidor, o browser abrirá uma nova aba do projeto em `http://localhost:4200`


Abra uma nova aba do terminal e inicie a API fake

```
npm run api
```


## Tecnologias usadas

### Angular 6

Os melhores frameworks JS atualmente entregam o mesmo conceito, como modularização, reuso de componentes, injeção de dependência, controle de estado da aplicação, SPA, PWA, etc. Com Angular não é diferente, além de entregar todos estes conceitos, ele possui um legado de recursos extenso que visa o desenvolvimento rápido e crescimento da aplicação, mas trás como contra a curva de aprendizagem. 

Podemos ter muitas vantagens usando Angular para desenvolvimento de aplicações complexas, como exemplo, uma loja virtual integrada com Magento2, que possui *responses* bem grandes que precisam ser modelados e bem tratados de forma assíncrona.

### Angular Client

Angular CLI segue as práticas de design parttens do Angular e auxilia na construção da estrutura e injeção de dependência automática dentro dos módulos. Todos os comandos usados para criar a estrutura do projeto estão em `docs/cheatsheet.md`.

### JSON Server

[https://github.com/typicode/json-server](JSON server) fornece API fake REST full com diferentes endpoints, usado em nosso projeto para construir a lista de categorias e de produtos,  podendo receber *request* com parâmetros de quantidade e ordenação. Desta forma teremos uma API mais próxima do real para criar nosso catálgo.

Para iniciar a API fake do projeto, execute  o comando `npm run api` em uma nova aba do terminal. A API será iniciada e poderá ser visualizada em `http://localhost:4300`.

### TypeScript

Adotado como padrão no Angular, ganhamos agilidade no desenvolvimento para validação instantânea no código usando VScode, tipagem das interfaces dos objetos de categoria e produto e tipagem dos parâmetros dos principais métodos de serviço.

### Karma

[https://github.com/karma-runner/karma](Karma) é instalado como dependência padrão pelo Angular CLI. Usado para realizar testes reais do código da aplicação usando os browsers em que o projeto esta aberto. Execute o comando `npm run test` no terminal.

### Bootstrap 4

Usado de forma modular, importando apenas os componentes usados no projeto e sobrescrevendo *variables*.
