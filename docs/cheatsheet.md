# Comandos CLI

Sequencia de comandos usados para construir a estrutura da aplicação.

### Angular: Init

`ng n jump --style=sass`

```
ng g m layout -m=app && \
ng g c layout/navbar --selector=app-navbar -m=layout --export=true && \
ng g c layout/header --selector=app-header -m=layout --export=true && \
ng g c layout/footer --selector=app-footer -m=layout --export=true
```

```
ng g m routing --flat --module=app

```

### Angular: Pages
```
ng g m pages -m=app && \
ng g c pages/home     --selector=page-home     -m=pages && \
ng g c pages/product  --selector=page-product  -m=pages && \
ng g c pages/category --selector=page-category -m=pages && \
ng g c pages/cart     --selector=page-cart     -m=pages && \
ng g c pages/checkout --selector=page-checkout -m=pages && \
ng g c pages/login    --selector=page-login    -m=pages && \
ng g c pages/register --selector=page-register -m=pages && \
ng g c pages/error404 --selector=page-error404 -m=pages
```

### Components

`ng g m components -m=app`

Components de **navegação**

```
ng g m components/navigation -m=components && \
ng g c components/navigation/filters    --selector=navigation-filters    -m=components/navigation --export=true && \
ng g c components/navigation/toolbar    --selector=navigation-toolbar    -m=components/navigation --export=true && \
ng g c components/navigation/pagination --selector=navigation-pagination -m=components/navigation --export=true && \
ng g c components/navigation/breadcrumb --selector=navigation-breadcrumb -m=components/navigation --export=true && \
ng g c components/navigation/search     --selector=navigation-search     -m=components/navigation --export=true
```

Components de **produto**

```
ng g m components/product -m=components && \
ng g c components/product/thumbnail   --selector=product-thumbnail   -m=components/product --export=true && \
ng g c components/product/card       --selector=product-card         -m=components/product --export=true && \
ng g c components/product/gallery     --selector=product-gallery     -m=components/product --export=true && \
ng g c components/product/description --selector=product-description -m=components/product --export=true && \
ng g c components/product/reviews     --selector=product-reviews     -m=components/product --export=true && \
ng g c components/product/related     --selector=product-related     -m=components/product --export=true
```

Components de **catálogo**

```
ng g m components/catalog -m=components && \
ng g c components/catalog/list     --selector=catalog-list     -m=components/catalog --export=true && \
ng g c components/catalog/carousel --selector=catalog-carousel -m=components/catalog --export=true
```

### Services

Consulta do catálogo na API

```
ng g s services/categories/categories && \
ng g s services/products/products && \
ng g i services/categories/category && \
ng g i services/products/product
```